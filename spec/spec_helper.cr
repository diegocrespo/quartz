require "spec"
require "../src/quartz"

def create_op(arr : Array(UInt8)) : UInt16
  high_byte : UInt16 = arr[0].to_u16
  low_byte : UInt16 = arr[1].to_u16
  op : UInt16 = ((high_byte << 8) | low_byte).to_u16
end
