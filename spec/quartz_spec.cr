require "./spec_helper"
require "spec"

describe "#interpret" do
  it "handles 0x1000 correctly" do
    mem = [0x15_u8, 0x2A_u8]
    op = create_op(mem)
    chip = Chip8.new("dummy", mem)
    chip.interpret

    chip.pc.should eq (op & 0x0FFF)    
  end

  it "handles 0x2000 correctly" do
    mem = [0x20_u8, 0x2A_u8]
    op = create_op(mem)    
    chip = Chip8.new("dummy", mem)
    old_sp = chip.sp
    chip.interpret
    
    chip.pc.should eq (op & 0x0FFF)
    chip.sp.should eq (old_sp + 1)    
  end

  it "handles 0x3000 correctly" do
    mem = [0x30_u8, 0x2A_u8]
    op = create_op(mem)        
    chip = Chip8.new("dummy", mem)
    old_pc = chip.pc
    low_byte = (op & 0x00FF).to_u8
    index = (op & 0x0F00) >> 8
    chip.reg[index] = low_byte
    chip.interpret

    chip.pc.should eq (old_pc + 2)
  end

  it "handles 0x4000 correctly" do
    #0x4013
    mem = [0x40_u8, 0x13_u8]
    op = create_op(mem)
    chip = Chip8.new("dummy", mem)
    old_pc = chip.pc
    low_byte = (op & 0x00FF).to_u8
    chip.interpret

    chip.pc.should eq (old_pc + 2)
  end

  it "handles 0x5000 correctly" do
    #0x5120
    # all regs are initialized to zero so PC is expectd to increment
    mem = [0x51_u8, 0x20_u8]
    op = create_op(mem)    
    chip = Chip8.new("dummy", mem)
    old_pc = chip.pc
    chip.interpret
    chip.pc.should eq (old_pc + 2)
  end

  it "handles 0x5000 correctly when they aren't equal" do
    #0x5130
    # all regs are initialized to zero so PC is expected to increment
    mem = [0x51_u8, 0x3_u8]
    op = create_op(mem)
    chip = Chip8.new("dummy", mem)
    old_pc = chip.pc
    x = (op & 0x0F00) >> 8
    y = (op & 0x00F0) >> 4
    chip.reg[y] = 10
    chip.reg[x] = 12
    chip.interpret

    chip.pc.should eq old_pc
  end

  it "handles 0x6000 correctly" do
    #0x62AB
    # all regs are initialized to zero so PC is expected to increment
    mem = [0x62_u8, 0xAB_u8]
    op = create_op(mem)        
    chip = Chip8.new("dummy", mem)
    x = (op & 0x0F00) >> 8
    chip.interpret

    chip.reg[x].should eq (0x62AB_u16 & 0x00FF)
  end  
  it "handles 0x7000 correctly" do
    #0x7F13
    # all regs are initialized to zero so PC is expected to increment
    mem = [0x7F_u8, 0x13_u8]
    op = create_op(mem)
    chip = Chip8.new("dummy", [0x7F_u8, 0x13_u8])
    x = (op & 0x0F00) >> 8
    reg_val = chip.reg[x]
    answer  = reg_val + ((op & 0x00FF).to_u8)
    chip.interpret

    chip.reg[x].should eq answer
  end
  it "handles 0x8000 correctly" do
    #0x8000
    mem = [0x80_u8, 0x00_u8]
    op = create_op(mem)    
    chip = Chip8.new("dummy", mem)
    x = (op & 0x0F00) >> 8
    y = (op & 0x00F0) >> 4
    chip.interpret
    chip.reg[x].should eq chip.reg[y]
  end

  it "handles 0x8001 correctly" do
    #0x8001
    mem = [0x80_u8, 0x01_u8]
    op = create_op(mem)        
    chip = Chip8.new("dummy", mem)
    x = (op & 0x0F00) >> 8
    y = (op & 0x00F0) >> 4
    chip.interpret

    chip.reg[x].should eq (chip.reg[x] | chip.reg[y])
  end

  it "handles 0x8002 correctly" do
    #0x8002
    mem = [0x80_u8, 0x02_u8]
    op = create_op(mem)      
    chip = Chip8.new("dummy", mem)
    x = (op & 0x0F00) >> 8
    y = (op & 0x00F0) >> 4
    chip.interpret

    chip.reg[x].should eq (chip.reg[x] & chip.reg[y])
  end      


  it "handles 0x8003 correctly" do
    #0x8003
    mem = [0x80_u8, 0x03_u8]
    op = create_op(mem)    
    chip = Chip8.new("dummy", mem)
    x = (op & 0x0F00) >> 8
    y = (op & 0x00F0) >> 4
    chip.interpret

    chip.reg[x].should eq (chip.reg[x] ^ chip.reg[y])
  end

  it "handles 0x8004 correctly" do
    #0x8124
    mem = [0x81_u8, 0x24_u8]
    op = create_op(mem)    
    chip = Chip8.new("dummy", mem)
    x = (op & 0x0F00) >> 8
    y = (op & 0x00F0) >> 4
    reg_sum = chip.reg[x] + chip.reg[y]
    chip.interpret
    chip.reg[x].should eq reg_sum
    chip.reg[0xF].should eq 0_u8
  end        

  it "handles 0x8004 carry correctly" do
    #0x8004
    mem = [0x8E_u8, 0xD4_u8]
    op = create_op(mem)    
    chip = Chip8.new("dummy", mem)
    x = (op & 0x0F00) >> 8
    y = (op & 0x00F0) >> 4
    # force the overflow
    chip.reg[x] = 255
    chip.reg[y] = 5
    # fake the overflow. Compiler won't allow arithmetic overflow
    reg_sum = (chip.reg[x].to_u16 + chip.reg[y].to_u16) % 256
    puts "the value of reg_sum is #{reg_sum}"
    chip.interpret
    chip.reg[x].should eq reg_sum
    chip.reg[0xF].should eq 1_u8    
  end        

end

