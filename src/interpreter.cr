MAX_MEMORY  =   4096
MASK_12_BIT = 0x0FFF
MASK_8_BIT  = 0x00FF
MASK_4_BIT  = 0x000F
MASK_X_REG  = 0x0F00
MASK_Y_REG  = 0x00F0
OP_SIZE = 2
# The chip object. Contains all the memory and registers of the Chip-8
struct Chip8
  # 4KB of RAM starting at 0x000 to 0xFFF
  # First 512B (0x000 to 0x1FF) are for interpreter and shouldn't be used
  # property mem = Array(UInt8).new(MAX_MEMORY, 0)
  property mem : Array(UInt8) = Array(UInt8).new(MAX_MEMORY, 0_u8)
  # This will store how large the actual file is
  property mem_size : Int32 = 0
  # 16 general purpose 8Bit registers
  property reg = Array(UInt8).new(16, 0)
  # 16Bit I register
  property i_reg : UInt16 = 0
  # special purpose 8Bit delay register. Decremented at 60Hz
  property delay_reg : UInt8 = 0
  # special purpose 8Bit sound timer. Decremented at 60Hz
  property sound_reg : UInt8 = 0
  # 16Bit program counter
  property pc : UInt16 = 0
  # 8Bit stack pointer
  property sp : UInt8 = 0
  # 16 16bit stack values
  property stack = Array(UInt16).new(16, 0)
  # TODO I'll get back to these
  keyboard = 0
  display = 0

  def initialize(file_name, test = Array(UInt8).new)
    if test.empty?
      load_file(file_name: file_name)
    else
      load_memory(data: test)
    end
  end

  # Use for manually loading memory values for testing
  def load_memory(data : Array(UInt8))
    data.each_with_index do |value, index|
      @mem[index] = value
    end
    @mem_size = data.size
  end

  # reads in the binary file with chip8 instructions and loads it into memory
  def load_file(file_name)
    if !File.exists?(file_name)
      raise File::NotFoundError.new("#{file_name} not found", file: file_name)
    elsif File.size(file_name) > MAX_MEMORY
      raise("File size exceeds maximum allowed size of #{MAX_MEMORY} bytes.")
    end
    File.open(file_name, "rb") do |file|
      index = 0
      while byte = file.read_byte
        @mem[index] = byte.to_u8
        index += 1
      end
      @mem_size = index
    end
  end

  def interpret
    0.step(to: @mem_size - 1, by: 2) do |x|
      high_byte : UInt16 = @mem.[x].to_u16
      low_byte : UInt16 = @mem[x + 1].to_u16
      op : UInt16 = ((high_byte << 8) | low_byte).to_u16
      printf("The op is %X\n", op)
      case op & 0xF000
      # nnn or addr - A 12-bit value, the lowest 12 bits of the instruction
      # n or nibble - A 4-bit value, the lowest 4 bits of the instruction
      # x - A 4-bit value, the lower 4 bits of the high byte of the instruction
      # y - A 4-bit value, the upper 4 bits of the low byte of the instruction
      # kk or byte - An 8-bit value, the lowest 8 bits of the instruction
      when 0x1000
        # 1nnn
        # set the program counter to lower 3 bytes
        @pc = op & MASK_12_BIT
      when 0x2000
        # 2nnn
        # interpreter increments the stack pointer
        # put current PC on the top of the stack
        # PC set to lower 12 bits of op
        @sp += 1
        @stack[@sp] = @pc
        @pc = op & MASK_12_BIT
      when 0x3000
        # 3xkk
        # Skip next instruction if Vx = kk.
        # Compares register Vx to kk
        # if they are equal, increments the program counter by 2.
        low_byte = op & MASK_8_BIT
        # register index
        index = (op & MASK_X_REG) >> 8

        if @reg[index] == low_byte
          @pc += OP_SIZE
        else
        end
      when 0x4000
        # 4xkk
        # Skip next instruction if Vx != kk.
        # Compares register Vx to kk
        # If they are not equal, increments the program counter by 2.
        low_byte = op & MASK_8_BIT
        # register index
        index = (op & MASK_X_REG) >> 8
        if @reg[index] != low_byte
          @pc += OP_SIZE
        end
      when 0x5000
        # 5xy0
        # Skip next instruction if Vx = Vy.
        # Compares register Vx to register Vy
        # if they are equal, increments the program counter by 2.
        x = (op & MASK_X_REG) >> 8
        y = (op & MASK_Y_REG) >> 4

        # register index
        if @reg[x] == @reg[y]
          @pc += OP_SIZE
        end
      when 0x6000
        # 6xkk
        # Set Vx = kk.
        low_byte = op & MASK_8_BIT
        x = (op & MASK_X_REG) >> 8
        @reg[x] = low_byte.to_u8
        @pc += OP_SIZE
      when 0x7000
        # 7xkk
        # Set Vx = Vx + kk.
        low_byte = op & MASK_8_BIT
        x = (op & MASK_X_REG) >> 8
        @reg[x] = @reg[x] + low_byte.to_u8
        @pc += OP_SIZE
      when 0x8000
        # have to check least significant bit
        # to determine what to do with ops in the
        # 0x8000 - 0x800E range
        case op & 0x000F
        when 0x0000
          # 8xy0
          # Set Vx = Vy.
          x = (op & MASK_X_REG) >> 8
          y = (op & MASK_Y_REG) >> 4
          @reg[x] = @reg[y]
          @pc += OP_SIZE
        when 0x0001
          # 8xy1
          # Set Vx = Vx OR Vy.
          x = (op & MASK_X_REG) >> 8
          y = (op & MASK_Y_REG) >> 4
          @reg[x] = @reg[x] | @reg[y]
          @pc += OP_SIZE
        when 0x0002
          # 8xy2
          # Set Vx = Vx AND Vy.
          x = (op & MASK_X_REG) >> 8
          y = (op & MASK_Y_REG) >> 4
          @reg[x] = @reg[x] & @reg[y]
          @pc += OP_SIZE
        when 0x0003
          # 8xy3
          # Set Vx = Vx XOR Vy.
          x = (op & MASK_X_REG) >> 8
          y = (op & MASK_Y_REG) >> 4
          @reg[x] = @reg[x] ^ @reg[y]
          @pc += OP_SIZE

        when 0x0004
          # 8xy4
          # Set Vx = Vx + Vy, set VF = carry.
          x = (op & MASK_X_REG) >> 8
          y = (op & MASK_Y_REG) >> 4
          sum : UInt16 = (@reg[x].to_u16 + @reg[y].to_u16)
          # the sum is greater than 8 bits 
          if sum > 255
            @reg[0xF] = 1_u8
            # fake the overflow behavior
            @reg[x] = (sum % 256).to_u8
          else
            @reg[0xF] = 0_u8
            @reg[x] = sum.to_u8
          end
         
          @pc += OP_SIZE
        when 0x0005
          # 8xy5
          # if Vx > Vy Vf = 1
          # else VF = 0
          # Set Vx = Vx - Vy
          x = (op & MASK_X_REG) >> 8
          y = (op & MASK_Y_REG) >> 4
          if @reg[x] > @reg[y]
            @reg[0xF] = 1_u8
          else
            @reg[0xF] = 0_u8
          end
          @reg[x] = (@reg[x] - @reg[y]).to_u8
          @pc += OP_SIZE
        when 0x0006
          # 8xy6
          # Set Vx = Vx SHR 1.
          x = (op & MASK_X_REG) >> 8
          if (@reg[x] & 0x1) == 1
            @reg[0xF] = 1_u8
          else
            @reg[0xF] = 0_u8
          end
          @reg[x] >>= 1
          @pc += OP_SIZE
        when 0x0007
          # 8xy7
          # Set Vx = Vy - Vx, set VF = NOT borrow.
          x = (op & MASK_X_REG) >> 8
          y = (op & MASK_Y_REG) >> 4
          if @reg[y] > @reg[x]
            @reg[0xF] = 1_u8
          else
            @reg[0xF] = 0_u8
          end
          @reg[x] = (@reg[y] - @reg[x]).to_u8
          @pc += OP_SIZE
        when 0x000E
          # 8xyE
          # Set Vx = Vx SHL 1.
          x = (op & MASK_X_REG) >> 8
          if (@reg[x] & 0x80) == 0x80
            @reg[0xF] = 1_u8
          else
            @reg[0xF] = 0_u8
          end
          @reg[x] <<= 1
          @pc += 2          
        end
      else
        printf("I can't understand the op %X\n", op)
      end
    end
  end
end

# chip = Chip8.new("output.bin")

# puts "the mem values are #{chip.mem[0..chip.mem_size - 1]}"
# chip.interpret
